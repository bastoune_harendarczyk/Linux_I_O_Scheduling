#!/bin/bash

cd Microbenchmarks-Streaming-Writes-and-Reads
cat /sys/block/sda/queue/scheduler
echo -e "\nDEADLINE\n"

./script.sh && ./script2.sh &
wait
echo -e "\n"


echo noop > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nNOOP\n"

./script.sh && ./script2.sh &
wait
echo -e "\n"

echo cfq > /sys/block/sda/queue/scheduler
cat /sys/block/sda/queue/scheduler
echo -e "\nCFQ\n"

./script.sh && ./script2.sh &
wait
echo -e "\n"
